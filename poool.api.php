<?php

/**
 * @file
 * Poool API documentation.
 */

/**
 * Perform alterations on page_match.
 *
 * Let other modules override the poool page type.
 *
 * @param string $page_match
 *   The poool page type (eg. 'page', 'free', 'premium', 'subscription',
 * 'registration'). Default: 'page'.
 */
function hook_poool_page_types_pages_alter(&$page_match) {
  // In case we are on example node bundle, check field
  // field_example_test.
  $page_entity = _poool_get_page_entity();
  if ($page_entity === NULL) {
    return;
  }
  if ($page_entity->getEntityTypeId() === 'node' && $page_entity->bundle() === 'example') {
    // Set this page as free.
    if ($page_entity->get('field_example_test')->getString() !== '0') {
      $page_match = 'free';
    }
  }
}
