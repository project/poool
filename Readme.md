CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The <strong>Poool</strong> module implements the <a href="https://poool.tech/">Poool</a> paywall solution.

REQUIREMENTS
------------

  * Get a poool subscription. 


INSTALLATION
------------

 * Install this module normally, just like you would install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Web services > Poool and
       configure your implementation.
    3. Optionnaly, you can alter some data and configurations thanks to alters.
       See poool.api.php for more details.


OVERRIDE COOKIES AUTHORIZATION BY JAVASCRIPT
--------------------------------------------

For RGPD considerations, you can send to poool if cookies have been allowed.
Here is an example for Matomo configuration with an audience group.

<code>
(function($, Drupal, drupalSettings) {
    Drupal.behaviors.withMatomo = {
        attach: function(context, settings) {
          Drupal.eu_cookie_compliance('postPreferencesLoad', function(){
            if (Drupal.eu_cookie_compliance.hasAgreed('audience')) {
              // Tell poool it's allowed to track with cookies.
              drupalSettings.poool.config.cookies_enabled = true;
            }
          });
        }
    };
})(jQuery, Drupal, drupalSettings);
</code>


DEFINE IF USER IS PREMIUM
-------------------------

To define if user is a premium user, subscribe to
`PooolEvents::POOOL_USER_IS_PREMIUM` and return `TRUE` if user is premium.

<code>
<?php

namespace Drupal\iexample\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\poool\Event\PooolEvents;
use Drupal\poool\Event\PooolUserIsPremiumEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PooolSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * PooolSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    $events[PooolEvents::POOOL_USER_IS_PREMIUM][] = [
      'defineUserIsPremium'
    ];

    return $events;
  }

  /**
   * Define if user is premium.
   *
   * @param \Drupal\poool\Event\PooolUserIsPremiumEvent $event
   *   The event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function defineUserIsPremium(PooolUserIsPremiumEvent $event) {
    $user = $this->entityTypeManager->getStorage('user')->load($event->getAccount()->id());

    // Put hereyour code to do whatever you want to define if a user is premium.
    // ...

    // Define this user as premium user.
    $event->setPremium(TRUE);
  }

}
</code>


MAINTAINERS
-----------

 * Fabien Clément (Goz) - https://www.drupal.org/u/goz

Supporting organizations:

 * Iloofo  - https://www.drupal.org/iloofo
