<?php

namespace Drupal\poool;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\poool\Event\PooolEvents;
use Drupal\poool\Event\PooolUserIsPremiumEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class PooolService
 *
 * @package Drupal\poool
 */
class PooolHelperService {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * PooolHelperService constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  public function __construct(EventDispatcherInterface $eventDispatcher) {
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * Does the user is premium.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The user account.
   *
   * @return bool
   */
  public function userIsPremium(AccountProxyInterface $account) {
    $event = new PooolUserIsPremiumEvent($account);
    $this->eventDispatcher->dispatch($event, PooolEvents::POOOL_USER_IS_PREMIUM);
    return $event->isPremium();
  }

}
