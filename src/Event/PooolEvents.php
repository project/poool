<?php

namespace Drupal\poool\Event;

/**
 * Defines events for the Poool module.
 */
final class PooolEvents {

  /**
   * Name of the event fired to know if user is premium.
   *
   * @Event
   *
   * @see \Drupal\poool\Event\PooolUserIsPremiumEvent
   */
  const POOOL_USER_IS_PREMIUM = 'poool.user_is_premium';

}
