<?php

namespace Drupal\poool\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Defines the pool user is premium event.
 *
 * @see \Drupal\poool\Event\PooolEvents
 */
class PooolUserIsPremiumEvent extends Event {

  /**
   * The user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Define if user is premium.
   *
   * @var bool
   */
  protected $premium = FALSE;

  /**
   * Constructs a new PooolUserIsPremiumEvent object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The account.
   */
  public function __construct(AccountProxyInterface $account) {
    $this->account = $account;
  }

  /**
   * Get the account.
   *
   * @return \Drupal\Core\Session\AccountProxyInterface
   *   The account.
   */
  public function getAccount(): AccountProxyInterface {
    return $this->account;
  }

  /**
   * Set the user premium information.
   *
   * @param bool $isPremium
   *   Set user as premium.
   */
  public function setPremium(bool $isPremium) {
    $this->premium = $isPremium;
  }

  /**
   * Know if user user is premium.
   *
   * @return bool
   *   TRUE if user is premium.
   */
  public function isPremium() {
    return $this->premium;
  }

}
