<?php

namespace Drupal\poool\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\poool\Manager\PooolManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Poool settings for this site
 *
 * @internal
 */
class PooolSettingsForm extends ConfigFormBase {

  /**
   * The Poool manager.
   *
   * @var \Drupal\poool\Manager\PooolManager
   */
  protected $pooolManager;

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\poool\Manager\PooolManager $poool_manager
   *   The poool manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PooolManager $poool_manager) {
    parent::__construct($config_factory);

    $this->pooolManager = $poool_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('poool.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'poool_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['poool.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('poool.settings');
    $form['application_identifier'] = [
      '#type' => 'textfield',
      '#title' => t('Application identifier'),
      '#default_value' => $config->get('application_id'),
      '#description' => t('Identifier of your application for poool.'),
    ];
    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable debug'),
      '#default_value' => $config->get('debug'),
      '#description' => t('Enable the debug option on poool.'),
    ];

    $form['tracking']['page_visibility_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Pages'),
      '#group' => 'tracking_scope',
    ];

    $form['tracking']['page_visibility_settings']['poool_visibility_request_path_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('By default, specify if tracking should be displayed or not on all pages.'),
      '#options' => [
        $this->t('Do not display on all pages'),
        $this->t('Display on all pages'),
      ],
      '#default_value' => $config->get('visibility.request_path_mode'),
    ];
    $description = $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", [
      '%blog' => '/blog',
      '%blog-wildcard' => '/blog/*',
      '%front' => '<front>',
    ]);
    $form['tracking']['page_visibility_settings']['poool_visibility_request_path_pages_visible'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Visible on pages'),
      '#default_value' => $config->get('visibility.request_path_pages_visible'),
      '#description' => $description,
      '#rows' => 10,
    ];
    $form['tracking']['page_visibility_settings']['poool_visibility_request_path_pages_not_visible'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Not visible on pages'),
      '#default_value' => $config->get('visibility.request_path_pages_not_visible'),
      '#description' => $description,
      '#rows' => 10,
    ];

    // Render the role overview.
    $visibility_user_role_roles = $config->get('visibility.user_role_roles');

    $form['tracking']['role_visibility_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles'),
      '#group' => 'tracking_scope',
    ];

    $form['tracking']['role_visibility_settings']['poool_visibility_user_role_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add tracking for specific roles'),
      '#options' => [
        $this->t('Add to the selected roles only'),
        $this->t('Add to every role except the selected ones'),
      ],
      '#default_value' => $config->get('visibility.user_role_mode'),
    ];
    $form['tracking']['role_visibility_settings']['poool_visibility_user_role_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#default_value' => !empty($visibility_user_role_roles) ? $visibility_user_role_roles : [],
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', user_role_names()),
      '#description' => $this->t('If none of the roles are selected, all users will be tracked. If a user has any of the roles checked, that user will be tracked (or excluded, depending on the setting above).'),
    ];

    $form['tracking']['conversion_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Conversion on pages'),
      '#group' => 'tracking_conversion_scope',
    ];
    $form['tracking']['conversion_settings']['poool_conversion_request_path_pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Set conversion on pages'),
      '#title_display' => 'invisible',
      '#default_value' => $config->get('conversion.request_path_pages'),
      '#description' => $description,
      '#rows' => 10,
    ];

    $form['tracking']['page_types_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Page types'),
      '#group' => 'tracking_page_types_scope',
    ];
    $types = self::pooolPageTypes();
    $bundles_options = [];
    foreach ($this->pooolManager->getContentEntityTypes() as $entity_type_id => $entity_info) {
      if ($entity_info->hasKey('bundle')) {
        $bundles = $this->pooolManager->getBundles($entity_type_id);

        foreach ($bundles as $bundle => $bundle_info) {
          $bundles_options[$entity_type_id][$entity_type_id . '__' . $bundle] = $entity_info->getLabel() . ' - ' . $bundle_info['label'];
        }
      }
      else {
        $bundles_options[$entity_type_id][$entity_type_id . '__default'] = $entity_info->getLabel() . ' - (default)';
      }
    }

    foreach ($types as $type => $type_label) {
      $form['tracking']['page_types_settings'][$type] = [
        '#type' => 'details',
        '#title' => $type_label,
        '#group' => 'tracking_page_types_scope_' . $type,
        'poool_page_types_' . $type . '_request_path_pages' => [
          '#type' => 'textarea',
          '#title' => $this->t('Set page as @label', ['@label' => $type_label]),
          '#title_display' => 'invisible',
          '#default_value' => $config->get('page_types.' . $type . '.request_path_pages'),
          '#description' => $description,
          '#rows' => 10,
        ],
        'poool_page_types_' . $type . '_entity_bundle' => [
          '#type' => 'select',
          '#title' => $this->t('Entity bundles'),
          '#description' => $this->t('Select the bundles which will send this page type if then entity bundle content is displayed in route path.'),
          '#multiple' => TRUE,
          '#options' => $bundles_options,
          '#default_value' => $config->get('page_types.' . $type . '.entity_bundle_pages'),
          '#size' => '10',
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Check application id validitiy.
    $application_id = $form_state->getValue('application_identifier');
    if (!preg_match('/^((([A-Z0-9]{5}-){3}[A-Z0-9]{5})|([A-Za-z0-9]{64}))$/', $application_id)) {
      $form_state->setErrorByName('application_identifier', $this->t('The application identifier is not valid.'));
    }

    $form_state->setValue('poool_visibility_request_path_pages_visible', trim($form_state->getValue('poool_visibility_request_path_pages_visible')));
    $form_state->setValue('poool_visibility_request_path_pages_not_visible', trim($form_state->getValue('poool_visibility_request_path_pages_not_visible')));
    $form_state->setValue('poool_visibility_user_role_roles', array_filter($form_state->getValue('poool_visibility_user_role_roles')));
    $form_state->setValue('poool_conversion_request_path_pages', trim($form_state->getValue('poool_conversion_request_path_pages')));
    $types = array_keys(self::pooolPageTypes());
    foreach ($types as $type) {
      $form_state_key = 'poool_page_types_' . $type . '_request_path_pages';
      $form_state->setValue($form_state_key, trim($form_state->getValue($form_state_key)));

      if (!empty($form_state->getValue($form_state_key))) {
        $pages = preg_split('/(\r\n?|\n)/', $form_state->getValue($form_state_key));
        foreach ($pages as $page) {
          if (strpos($page, '/') !== 0 && $page !== '<front>') {
            $form_state->setErrorByName($form_state_key, $this->t('Path "@page" not prefixed with slash.', ['@page' => $page]));
            // Drupal forms show one error only.
            break;
          }
        }
      }
    }

    // Verify that every path is prefixed with a slash, but do not check for
    // slashes if no paths configured.
    if (!empty($form_state->getValue('poool_visibility_request_path_pages_visible'))) {
      $pages = preg_split('/(\r\n?|\n)/', $form_state->getValue('poool_visibility_request_path_pages_visible'));
      foreach ($pages as $page) {
        if (strpos($page, '/') !== 0 && $page !== '<front>') {
          $form_state->setErrorByName('poool_visibility_request_path_pages_visible', $this->t('Path "@page" not prefixed with slash.', ['@page' => $page]));
          // Drupal forms show one error only.
          break;
        }
      }
    }
    if (!empty($form_state->getValue('poool_visibility_request_path_pages_not_visible_visible'))) {
      $pages = preg_split('/(\r\n?|\n)/', $form_state->getValue('poool_visibility_request_path_pages_not_visible_visible'));
      foreach ($pages as $page) {
        if (strpos($page, '/') !== 0 && $page !== '<front>') {
          $form_state->setErrorByName('poool_visibility_request_path_pages_not_visible_visible', $this->t('Path "@page" not prefixed with slash.', ['@page' => $page]));
          // Drupal forms show one error only.
          break;
        }
      }
    }
    if (!empty($form_state->getValue('poool_conversion_request_path_pages'))) {
      $pages = preg_split('/(\r\n?|\n)/', $form_state->getValue('poool_conversion_request_path_pages'));
      foreach ($pages as $page) {
        if (strpos($page, '/') !== 0 && $page !== '<front>') {
          $form_state->setErrorByName('poool_conversion_request_path_pages', $this->t('Path "@page" not prefixed with slash.', ['@page' => $page]));
          // Drupal forms show one error only.
          break;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $poool_settings = $this->config('poool.settings');
    $types = array_keys(self::pooolPageTypes());
    foreach ($types as $type) {
      $poool_settings->set('page_types.' . $type . '.request_path_pages', $form_state->getValue('poool_page_types_' . $type . '_request_path_pages'));
      $poool_settings->set('page_types.' . $type . '.entity_bundle_pages', $form_state->getValue('poool_page_types_' . $type . '_entity_bundle'));
    }

    $poool_settings
      ->set('application_id', $form_state->getValue('application_identifier'))
      ->set('debug', $form_state->getValue('debug'))
      ->set('visibility.request_path_mode', $form_state->getValue('poool_visibility_request_path_mode'))
      ->set('visibility.request_path_pages_visible', $form_state->getValue('poool_visibility_request_path_pages_visible'))
      ->set('visibility.request_path_pages_not_visible', $form_state->getValue('poool_visibility_request_path_pages_not_visible'))
      ->set('visibility.user_role_mode', $form_state->getValue('poool_visibility_user_role_mode'))
      ->set('visibility.user_role_roles', $form_state->getValue('poool_visibility_user_role_roles'))
      ->set('conversion.request_path_pages', $form_state->getValue('poool_conversion_request_path_pages'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Get the poool page types except the default page.
   *
   * @return array
   *   Array of Poool page types.
   */
  protected function pooolPageTypes(): array {
    return [
      'free' => $this->t('Free'),
      'premium' => $this->t('Premium'),
      'subscription' => $this->t('Subscription'),
      'registration' => $this->t('Registration'),
    ];
  }

}
