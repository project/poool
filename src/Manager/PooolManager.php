<?php

namespace Drupal\poool\Manager;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Manage poool configuration.
 */
class PooolManager {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The immutable poool configuration entity.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * PooolManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * Get all content entity types.
   *
   * @return \Drupal\Core\Entity\ContentEntityTypeInterface[]
   */
  public function getContentEntityTypes() {
    $definitions = $this->entityTypeManager->getDefinitions();
    $ret = [];
    foreach ($definitions as $machine => $type) {
      if ($type instanceof ContentEntityTypeInterface) {
        $ret[$machine] = $type;
      }
    }

    return $ret;
  }

  /**
   * Get all bundle of an entity type.
   *
   * @param string $entity_type
   *   The entity type id.
   *
   * @return array
   */
  public function getBundles($entity_type) {
    return $this->entityTypeBundleInfo->getBundleInfo($entity_type);
  }

  /**
   * Get poool field types for an entity.
   *
   * @param ContentEntityInterface $entity
   *   The content entity.
   *
   * @return array
   */
  public function getPooolFields(ContentEntityInterface $entity): array {
    $field_list = [];

    // Get a list of the metatag field types.
    $field_types = self::fieldTypes();

    // Get a list of the field definitions on this entity.
    $definitions = $entity->getFieldDefinitions();

    // Iterate through all the fields looking for ones in our list.
    foreach ($definitions as $field_name => $definition) {
      // Get the field type, ie: metatag.
      $field_type = $definition->getType();

      // Check the field type against our list of fields.
      if (isset($field_type) && in_array($field_type, $field_types)) {
        $field_list[$field_name] = $definition;
      }
    }

    return $field_list;
  }

  /**
   * Get poool settings for an entity.
   *
   * @param ContentEntityInterface $entity
   *   The content entity to get settings.
   *
   * @return array
   *   The settings. Empty otherwise.
   */
  public function getPooolSettingsForEntity(ContentEntityInterface $entity): array {
    $settings = [];
    $fields = self::getPooolFields($entity);
    /* @var \Drupal\field\Entity\FieldConfig $field_info */
    foreach ($fields as $field_name => $field_info) {
      // Get the tags from this field.
      $settings += self::getFieldSettings($entity, $field_name);
    }
    return $settings;
  }

  /**
   * Get poool field settings.
   *
   * @param ContentEntityInterface $entity
   *   The content entity.
   * @param string $field_name
   *   The field name.
   *
   * @return array
   *   The settings.
   */
  public function getFieldSettings(ContentEntityInterface $entity, string $field_name): array{
    $settings = [];
    foreach ($entity->{$field_name} as $item) {
      // Get serialized value and break it into an array of tags with values.
      $serialized_value = $item->get('value')->getValue();
      if (!empty($serialized_value)) {
        $settings += unserialize($serialized_value);
      }
    }

    return $settings;
  }

  /**
   * Set default pool settings for pool field type.
   *
   * @return array
   *   The default settings.
   */
  public static function fieldDefaultSettings() {
    return [
      'mode' => 'hide',
      'percent' => 80,
      'page-type' => 'premium',
    ];
  }

  /**
   * Set default pool settings for pool field type.
   *
   * @return array
   *   The default settings.
   */
  public static function fieldTypes() {
    return [
      'poool',
    ];
  }

}
