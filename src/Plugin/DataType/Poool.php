<?php

namespace Drupal\poool\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\StringData;

/**
 * The poool data type.
 *
 * The plain value of a poool is a serialized object represented as a string.
 *
 * @DataType(
 *  id = "poool",
 *  label = @Translation("Poool")
 * )
 */
class Poool extends StringData implements PooolInterface {

}
