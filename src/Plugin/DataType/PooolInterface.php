<?php

namespace Drupal\poool\Plugin\DataType;

use Drupal\Core\TypedData\PrimitiveInterface;

/**
 * The poool data type.
 *
 * The plain value of a poool is a serialized object represented as a string.
 */
interface PooolInterface extends PrimitiveInterface {

}
