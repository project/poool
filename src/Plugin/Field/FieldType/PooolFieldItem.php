<?php

namespace Drupal\poool\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'poool' field type.
 *
 * @FieldType(
 *   id = "poool",
 *   label = @Translation("Poool"),
 *   description = @Translation("This field stores poool settings."),
 *   default_widget = "poool_default",
 *   default_formatter = "poool_empty_formatter",
 *   serialized_property_names = {
 *     "value"
 *   }
 * )
 */
class PooolFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('poool')
      ->setLabel(t('Poool'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '' || $value === serialize([]);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    /** @var \Drupal\poool\Manager\PooolManager $pooolManager */
    $pooolManager = \Drupal::service('poool.manager');
    // Get field defaults.
    $default_settings = $pooolManager->fieldDefaultSettings();

    // Get the value about to be saved.
    $current_value = $this->value;
    // Only unserialize if still serialized string.
    if (is_string($current_value)) {
      $current_settings = unserialize($current_value);
    }
    else {
      $current_settings = $current_value;
    }

    // Only include values that differ from the default.
    $settings_to_save = [];
    foreach ($current_settings as $setting_id => $setting_value) {
      if (!isset($default_settings[$setting_id]) || ($setting_value != $default_settings[$setting_id])) {
        $settings_to_save[$setting_id] = $setting_value;
      }
    }

    // Update the value to only save overridden settings.
    $this->value = serialize($settings_to_save);
  }

}
