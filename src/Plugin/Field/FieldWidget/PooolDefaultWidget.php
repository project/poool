<?php

namespace Drupal\poool\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\poool\Manager\PooolManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Advanced widget for poool field.
 *
 * @FieldWidget(
 *   id = "poool_default",
 *   label = @Translation("Advanced poool form"),
 *   field_types = {
 *     "poool"
 *   }
 * )
 */
class PooolDefaultWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The poool helper service.
   *
   * @var \Drupal\poool\Manager\PooolManager
   */
  protected $pooolManager;

  /**
   * {@inheritdoc}
   * @param \Drupal\poool\Manager\PooolManager $pooolManager
   *   The poool manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, PooolManager $pooolManager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->pooolManager = $pooolManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('poool.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'sidebar' => TRUE,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['sidebar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place field in sidebar'),
      '#default_value' => $this->getSetting('sidebar'),
      '#description' => $this->t('If checked, the field will be placed in the sidebar on entity forms.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    if ($this->getSetting('sidebar')) {
      $summary[] = $this->t('Use sidebar: Yes');
    }
    else {
      $summary[] = $this->t('Use sidebar: No');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];
    $default_settings = $this->pooolManager->fieldDefaultSettings();

    // Retrieve the values for each poool from the serialized array.
    $values = [];
    if (!empty($item->value)) {
      $values = unserialize($item->value);
    }

    // Populate fields which have not been overridden in the entity.
    if (!empty($default_settings)) {
      foreach ($default_settings as $setting_id => $setting_value) {
        if (!isset($values[$setting_id]) && !empty($setting_value)) {
          $values[$setting_id] = $setting_value;
        }
      }
    }

    $element += [
      '#type' => 'details',
      'page-type' => [
        '#type' => 'select',
        '#title' => $this->t('Page type'),
        '#options' => [
          'free' => $this->t('Free'),
          'premium' => $this->t('Premium'),
          'subscription' => $this->t('Subscription'),
          'registration' => $this->t('Registration'),
        ],
        '#description' => $this->t('The poool page type.'),
        '#default_value' => $values['page-type'],
      ],
      'mode' => [
        '#type' => 'select',
        '#title' => $this->t('Mode'),
        '#options' => [
          'hide' => $this->t('Hide'),
          'excerpt' => $this->t('Excerpt'),
          'custom' => $this->t('Custom'),
        ],
        '#description' => $this->t('This is the method used by Poool to lock the content of your article. Available modes: `hide`: Your original content will stay where it is, but its container will be modified with css to hide a percentage of the content. Downsides : Users will be able to open the browser\'s console and remove the css rules. `excerpt`: Your original content will be placed in a view holder and its container will have its content replaced by trimmed text (only X% of the text will be kept). The original content is not available from outside of Poool.js scope (even from console), and will be put back when onRelease event is fired. Downsides : &lt;style&gt; and &lt;script&gt; tags inside parent are temporarily detached from trimmed text and put at the end. If you have various &lt;script&gt; tags in your content, they won\'t necessarily be executed and may even throw javascript errors (such as with some ad scripts that still use obsolete javascript functions), leading to a blank page when unlocking content. This mode is also not recommended for single-page apps (Angular, React, Vue, ...). In these two cases, we suggest that you use hide mode. `custom`: Nothing will be done to your content and/or its container, but onLock and onRelease events will be fired so you can do your own processing.'),
        '#default_value' => $values['mode'],
        '#states' => [
          'visible' => [
            'select[name="' . $items->getName() . '[' . $delta . '][page-type]"]' => ['value' => 'premium'],
          ],
        ],
      ],
      'percent' => [
        '#type' => 'number',
        '#title' => $this->t('Percent'),
        '#description' => $this->t('Percentage of text you want to be hidden/stripped'),
        '#default_value' => $values['percent'],
        '#states' => [
          'visible' => [
            'select[name="' . $items->getName() . '[' . $delta . '][page-type]"]' => ['value' => 'premium']
          ],
        ],
      ],
    ];

    // If the "sidebar" option was checked on the field widget, put the
    // form element into the form's "advanced" group. Otherwise, let it
    // default to the main field area.
    $sidebar = $this->getSetting('sidebar');
    if ($sidebar) {
      $element['#group'] = 'advanced';
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Flatten the values array to remove the groups and then serialize all the
    // meta settings into one value for storage.
    foreach ($values as &$value) {
      $value = serialize($value);
    }

    return $values;
  }

}
