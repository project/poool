<?php

namespace Drupal\poool\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'poool_empty_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "poool_empty_formatter",
 *   module = "poool",
 *   label = @Translation("Empty formatter"),
 *   field_types = {
 *     "poool"
 *   }
 * )
 */
class PooolEmptyFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Does not actually output anything.
    return [];
  }

}
