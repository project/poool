/**
 * @file
 * Attaches several event listener to a web page.
 */

(function ($, drupalSettings) {

  "use strict";

  $(document).ready(function () {
    if (drupalSettings.poool.id) {
      poool("init", drupalSettings.poool.id);
      drupalSettings.poool.config.cookies_enabled = drupalSettings.poool.config.cookies_enabled || false;
      Object.keys(drupalSettings.poool.config).map(function (key, index) {
        var value = drupalSettings.poool.config[key];
        poool("config", key, drupalSettings.poool.config[key]);
      });

      // Add cookies_domain to be sure complex domain will work.
      poool("config", "cookies_domain", window.location.hostname);

      // Define and send pageView.
      let pageView = drupalSettings.poool.pageview || "page";
      poool("send", "page-view", pageView);
      if (drupalSettings.poool.conversion) {
        poool("send", "conversion");
      }
    }
  });

})(jQuery, drupalSettings);
